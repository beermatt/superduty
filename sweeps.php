<?php session_start(); ?>

<!doctype html>
<html>
<head>
<base target="_parent" />
<meta charset="utf-8">
<title>2017 Ford Super Duty Drive Tour</title>
<link rel="stylesheet" type="text/css" href="styles/style.css"/>
<link rel="stylesheet" type="text/css" href="ford-fonts/stylesheet.css">
<link rel="stylesheet" type="text/css" href="styles/colorbox.css"/>
<script src="jquery/js/jquery-1.10.2.js"></script>
<script src="jquery/js/jquery-ui-1.10.4.custom.js"></script>
<script src="jquery/js/jquery.colorbox.js"></script>

<script>
$(document).ready(function(){
	$(".privacy").colorbox({iframe:true, width:"70%", height:"70%", opacity: 0.4});
});

$(document).ready(function(){
	$(".caprivacy").colorbox({iframe:true, width:"70%", height:"60%", opacity: 0.4});
});

$(document).ready(function(){
	$(".rules").colorbox({iframe:true, width:"70%", height:"60%", opacity: 0.4});
});
</script>

</head>

<body>

<div id="topbar"></div>

    	<div id="header">
        
            <div id="logo"></div>
        
            <div id="tagline"></div> 
        
        </div>

        <div id="formarea" style="height: 1200px; background-color: #222222;">
        
        	<div id="contentform" style="background-color: #fff; height: 1000px;">
            
            <?php include('page2.html'); ?>
            
            </div>
       
        </div>
        
      
	<?php include('includes/footer.php'); ?>  
    
</body>
</html>