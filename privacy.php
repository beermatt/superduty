<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="ford-fonts/stylesheet.css">
</head>

<body style="font-family:franklin_gothic_fsbook;">

<h1>2015 FORD VEHICLE SWEEPSTAKES</h1>
<h1>PRIVACY STATEMENT</h1>


<p>1.  General Statement:</p>
<p>We respect your privacy and are  committed to protecting it.  This privacy  statement explains our policies and practices regarding online customer  information.  It is through this  disclosure that we intend to provide you with a level of comfort and confidence  in how we collect, use, and safeguard personal and other information we collect  or that you provide through this website, and how you can contact us if you  have any questions or concerns.  It is  our sincere hope that by explaining our data handling practices we will develop  a trusting and long-lasting relationship with you.  By using the site, you agree to the terms of  this privacy statement.  </p>
<p>2.  Information About Our Organization and Website  and General Data Collection Practices:</p>
<p>This Privacy Statement applies to www.SuperDutyDrive.com.   SuperDutyDrive.com is administered by a  third-party vendor on behalf of Ford Motor Company, Marketing,  Sales and Service Division, 16800 Executive Plaza Drive, Dearborn, MI 48126.  </p>
<p>The business purpose of this website is to  provide you the opportunity to enter the 2017 Super Duty: Drive the Future of  Tough Sweepstakes, register for a Drive The Future of Tough event, learn more  about the 2017 Ford Super Duty truck, and where and when events will be held.  </p>
<p>For added convenience, a select set of site features  are accessible from web-enabled mobile devices as a mobile-optimized web  experience.&nbsp; These site features may be limited in functionality.&nbsp; </p>
<p>Online Tracking Information</p>
<p>When you visit this site  analytics providers may collect information about your online activity over  time and across websites. </p>
<p>Because there is not yet a  common understanding of how to interpret web browser-based &ldquo;Do Not Track&rdquo;  (&ldquo;DNT&rdquo;) signals other than cookies, Ford Motor Company does not currently  respond to undefined &ldquo;DNT&rdquo; signals to its US websites. </p>
<p>Most browsers can be configured  not to accept cookies, however, this may prevent you from having access to some  functions or services on our sites.</p>
<p>See Sections 5 and 7 for more  information.</p>
<p>3.  Personally Identifiable Information We  Collect From You:</p>
<p>When you visit SuperDutyDrive.com we will  not collect any personally identifiable information about you unless you  provide it to us voluntarily.  For purposes of this Privacy Statement, personally  identifiable information is information that identifies you as an individual such  as your name,  street address, telephone number, and email address.  If  you opt not to provide us with personally identifiable information you can  still access our website, however you will be unable to enter the Sweepstakes,  register for an event, or receive marketing or promotional materials  including emails. </p>
<p>Our primary goal in collecting  personally identifiable information from you when you visit SuperDutyDrive.com  is to provide you the functionality and services that you need to have a  meaningful and customized experience while using the site features.    </p>
<p>See Section 12 regarding links to  third-party sites, including widgets.</p>
<p>4.  How We Use  the Personally Identifiable Information We Collect:</p>
<p>Personally  identifiable information collected on SuperDutyDrive.com may be used to:</p>
<ul>
  <li>Fulfill a site user request (e.g., Sweepstakes  entry, event registration, brochure fulfillment, lead generation, send  marketing or promotional materials including emails)</li>
  <li>Respond to your comments or requests for  information</li>
  <li>Meet a request for or to develop new products or  services</li>
  <li>Make the user experience more customer friendly</li>
  <li>Develop new and improved products, services and  marketing</li>
  <li>Contact you if necessary in the course of  processing your entry or event registration</li>
  <li>Generate site analytics that improve our site  layout, content, product offerings and services</li>
  <li>Compile user data that is stored in our  corporate database and may be used for marketing and other purposes</li>
  <li>Match personal data collected here with data  about you that we collect through other sources</li>
  <li>Comply with legal requirements<br>
  </li>
</ul>
<p>5.  Other (Non-Personally Identifiable) Information We  Collect From Site Visitors:</p>
<p>SuperDutyDrive.com collects other &ldquo;non-personally  identifiable information&rdquo; from site visitors.   This means that we do not track these items on an individual basis that  identifies the site visitor, but rather gather and collect this information on  an aggregate or anonymous basis that includes all site visitors.  Non-personally identifiable information  includes tracking the site pages that are visited or the amount of time spent  on our site.   </p>
<p>If you visit this site using a mobile  device, we collect additional non-personally identifiable information which may  include the type/model of the mobile device viewing the site and the mobile  device&rsquo;s service provider and operating system. </p>
<p>Our primary goal in collecting non-personally  identifiable information from you is to be able to perform site metrics that allow  us to improve the functionality of the website.     </p>
<p>We may use session and persistent &ldquo;cookies,&rdquo;  session logs, spotlight ads/web beacons/GIF/pixel tags, banner ads or links, and  third-party click tracking analytics tools (such as Google Analytics) to  collect aggregate or other non-personally identifiable information about site  visitors.  An explanation on collection  methods and how they work is included in Section 7 - Methods to Collect Information, below.    </p>
<p>Google is independently owned and  operated, and data collected by Google is subject to its privacy policy.  We encourage you to review the Google privacy  policy at <a href="http://www.google.com/privacy.html">http://www.google.com/privacy.html</a> and Google&rsquo;s How Google uses  data when you use our partners' sites or apps (located at&nbsp;<a href="http://www.google.com/policies/privacy/partners/" target="_blank">www.google.com/policies/privacy/partners/</a>) so that you can understand how the  data is collected, used and shared.  </p>
<p>6.  How We Use the Aggregate or Non-Personally  Identifiable Information We Collect:</p>
<p>Non-Personally  identifiable information collected on SuperDutyDrive.com may be used to:</p>
<ul>
  <li>Compile aggregate and statistical data to help  in website design and to identify popular features</li>
  <li>Measure site activity to allow us to update our  site to better meet user wants and needs</li>
  <li>Provide you content that may be of interest to  you based on pages visited and items viewed</li>
  <li>Make the user experience more customer friendly</li>
  <li>Develop new and improved products, services and  marketing</li>
</ul>
<p>SuperDutyDrive.com utilizes non-personally identifiable information  for the purpose of performing analytics on the user&rsquo;s experience while visiting  this site. This analysis:</p>
<ul>
  <li>Is performed on an aggregate level and does not  identify you or your information personally</li>
  <li>May involve the use of a third-party vendor  acting on behalf of SuperDutyDrive.com</li>
  <li>May be combined with data collected by third  parties on other sites and apps and across devices</li>
  <li>Is performed in order to improve our website and  the user experience</li>
  <li>May include the use of session and/or persistent  cookies to track user movement across this and other Ford Motor Company  websites or to track other events within or across this and other Ford Motor  Company websites </li>
</ul>
<p>7.  Methods to  Collect Information:</p>
<p>A &ldquo;cookie&rdquo; is a small text file  that helps us in many ways to make your visit to our website more enjoyable and  meaningful to you. For example, cookies avoid you having to log in every time  you come back to one of our websites. They also allow us to tailor a website or  advertisement to better match your interests and preferences.  There  are a couple different types of cookies.</p>
<p>A &ldquo;session&rdquo; cookie is stored only in your computer&rsquo;s working memory  (RAM) and only lasts for your browsing session. When you close all your browser&rsquo;s  windows, or when you shut down your computer, the session cookie disappears  forever. </p>
<p>A &ldquo;persistent&rdquo; cookie is stored on your computer&rsquo;s hard drive until a  specified date, which could be tomorrow, next week, or 10 years from now.  Persistent cookies stay on your computer until either a) they expire, b) they  are overwritten with newer cookies, or c) you manually remove them.  Most browsers can be configured not to accept  cookies, however, this may prevent you from having access to some site  functions or features.    </p>
<p>This site may use third-party  click tracking analytics tools (such as Google Analytics) to capture  clickthrough statistics.  These parties  may collect information about your use of the site and across different sites  and mobile apps, and across devices over time.  </p>
<p>SuperDutyDrive.com may use spotlight  ads/web beacons/GIF/pixel tags which are site instrumentation tools that help us to  determine, for instance, whether a page has been viewed or not and, if so, how  many times.  Emails or electronic  newsletters we send may use tools (e.g., pixel tags or web beacons) to gather  email metrics and information to improve the reader&rsquo;s experience such as how  many of the emails are opened, if they were printed or forwarded, the type of  device (e.g., mobile or PC) from which they were opened, and the city, state,  and county associated with the applicable IP address.  In general, any electronic image viewed as  part of a webpage, including an ad banner, can act as a web beacon.    </p>
<p>This website may generate a  &ldquo;session log&rdquo; when you visit our site.  We  use session logs to help us determine how people travel through our site. In  this way, we can structure our pages so that the information most frequently  visited is easier to find. By tracking page visits, we can also determine if  the information we&rsquo;re providing is being used.   The only data gathered is the Internet Protocol (IP) address from which  you came, the website that referred you, the pages you visited and the date and  time of those visits.  </p>
<p>This site may provide visitors the  opportunity to integrate with third-party social media sites, and we may track  on an aggregate level things the number of items &ldquo;liked&rdquo; on this site, or items  on this site that you choose to share with a third-party social media  site.  </p>
<p>8.  Sharing  Your Information:</p>
<p>SuperDutyDrive.com does not share, sell, or rent personally  identifiable information with independent companies for their own use without  providing you a choice.</p>
<p>Personally identifiable  information that you provide to us in the course of requesting a product or  service through this website may be gathered and stored in one or more of our  corporate databases and be used for purposes of contacting you for things like  promotional offers, marketing programs, or other communications from this  website or other Ford Motor Company programs and services.  </p>
<p>We may share personally identifiable information that we  collect about you with other companies within the Ford Motor Company family of  companies or subsidiaries.  Our &ldquo;family  of companies&rdquo; is the group of companies related to us by common control or  ownership.  We share information within  this &ldquo;family&rdquo; as a normal part of conducting business and offering products and  services to our customers.  </p>
<p>Personally identifiable information that you provide to SuperDutyDrive.com  may be shared with our authorized dealers.  This is necessary, such as, in order to honor your request for a  price quote on a vehicle or in order to provide a dealer with information for  purposes of contacting you in their regular course of business.  The dealer is not restricted by SuperDutyDrive.com  in their use of your data.     </p>
<p>We may share personally  identifiable information  with vendors, contractors or partners in the normal course of business.  Vendors, contractors or partners of SuperDutyDrive.com  who have access to your personally identifiable information in connection with  providing services for SuperDutyDrive.com are required to keep the information  confidential and are not permitted to use this information for any other  purpose than to carry out the services they are performing for SuperDutyDrive.com.              </p>
<p>Site metrics for SuperDutyDrive.com  may be shared with other Ford Motor Company websites or subsidiaries or  affiliates.  </p>
<p>SuperDutyDrive.com will disclose your personally identifiable information,  without notice, if (1) in the good faith belief that such action is necessary  to: (a) conform to the edicts of the law or comply with legal process served on  Ford Motor Company, its affiliates or the site; (b) protect and defend the  rights or property of Ford Motor Company, its affiliates or this site; or (c)  act under exigent circumstances to protect the personal safety of Ford Motor  Company or affiliate personnel, users of their websites, or the public; or (2)  if required to do so by law.</p>
<p>Contest or sweepstakes entrants  only:  If you are providing personally  identifiable information in connection with a contest or sweepstakes through  this website, and if you are selected as a winner, we may include your  personally identifiable information in a publicly available winner&rsquo;s list.  See the applicable contest or sweepstakes  rules.  </p>
<p>9.  Access To  and Control Over the Use of Your Information:</p>
<p>To correct or update your personally  identifiable information contact us at     </p>
<p>Telephone<br>
  800-392-3673<br>
  800-232-5952 (TDD for the hearing impaired) <br>
  <br>
Available Monday-Friday, 8:00 a.m. - 8:00 p.m. EST and Saturday, 9:00  a.m. - 5:30 p.m. EST</p>
<p>  Mailing Address<br>
  Ford Motor Company<br>
  Customer Relationship Center<br>
  P.O. Box 6248<br>
Dearborn, MI 48126 </p>
<p>so that we may be able to process your changes.  SuperDutyDrive.com will use reasonable  efforts to correct any factual inaccuracies in your information.       </p>
<p>The scope of access available for this request is for  information stored in the database that holds the data for SuperDutyDrive.com.  Any personally identifiable information that  has been collected from you on other Company websites will not be accessible or  changeable through the processing of this request.</p>
<p>10.  Security  of Your Information:</p>
<p>Safeguarding information of visitors to our website is  important to us.  While no systems,  applications or websites are 100% secure, we use systems, policies and  procedures to maintain accuracy of information and to protect information from  loss, misuse or alteration.  </p>
<p>When you send personally identifiable information, such as email  address or telephone numbers, to SuperDutyDrive.com we use technologies such as  &ldquo;Secure Socket Layers&rdquo; (SSL) or &ldquo;Transport Layer Security (TLS) to protect your  data.  SSL and TLS are encryption  technologies that helps safeguard your personally identifiable information  against misuse by unauthorized individuals.              </p>
<p>Vendors, contractors or partners of SuperDutyDrive.com who  have access to your personally identifiable information in connection with  providing services for SuperDutyDrive.com are required to keep the information  confidential.</p>
<p>Vendors are not permitted to use this information for any  other purpose than to carry out the services they are performing for SuperDutyDrive.com.<br>
</p>
<p>11.   Processing of Personal Data:</p>
<p>By visiting this site and by providing your personally  identifiable information to us, you understand and consent to the collection,  use, processing, transfer, and disclosure of your personally identifiable and </p>
<p>non-personally identifiable information globally ---  including to the United States – in accordance with this Privacy  Statement.  Therefore, by visiting this  site and by providing such information, you consent to the transfer of such  information across country borders, and to the use, processing, and disclosure  of such information in global locations.   Your consent shall be deemed to include your consent to transfer of the  personally identifiable or non-personally identifiable information to locations  that may have different levels of privacy protection than in your own country.</p>
<p>12.  Links to  Other Sites:</p>
<p>SuperDutyDrive.com provides  links to other websites.  We encourage  you to review the privacy statements of all sites that you visit, including  those whose links are provided so that you can understand how those sites  collect, use and share your information.   SuperDutyDrive.com is not responsible for the privacy statements,  content or data handling practices on other websites.     </p>
<p>If you choose to utilize  the ShareThis widget to share content on this website with your friends, please  note that ShareThis is an independent third party and is not a vendor,  contractor or partner of SuperDutyDrive.com.  All information (personally identifiable or  aggregate) that is collected by the ShareThis widget is collected by ShareThis  and is subject to the ShareThis Privacy Policy and not the SuperDutyDrive.com Privacy  Statement.   </p>
<p>13.  Children&rsquo;s  Privacy:</p>
<p>SuperDutyDrive.com does not knowingly intend to collect  personally identifiable information from children under 13 years of age.  </p>
<p>If a child has provided us with personally identifiable  information a parent or guardian of that child may contact us at the phone  number or mailing address listed in section 14 if they want this information  deleted from our records.  We will then  make reasonable efforts to delete the child&rsquo;s information from the database  that stores information for SuperDutyDrive.com.  </p>
<p>14.  Contacting  Us:</p>
<p>If you have any questions,  comments or concerns about this online privacy statement for SuperDutyDrive.com,  SuperDutyDrive.com privacy practices, or have any questions regarding the  contents of this website please contact us at       </p>
<p>Telephone<br>
  800-392-3673<br>
  800-232-5952 (TDD for the hearing impaired) <br>
  <br>
  Available Monday-Friday, 8:00 a.m. - 8:00 p.m. EST and Saturday, 9:00  a.m. - 5:30 p.m. EST<br>
  <br>
  Mailing Address<br>
  Ford Motor Company<br>
  Customer Relationship Center<br>
  P.O. Box 6248<br>
Dearborn, MI 48126 </p>
<p><br>
SuperDutyDrive.com is committed to working with consumers to obtain a  fair and rapid resolution of any complaints or disputes about privacy and the  handling of your data.  SuperDutyDrive.com  will be happy to respond to your questions and comments.  </p>
<p>15.  Privacy  Statement Effective Date and Revision Days:</p>
<p>Occasionally we may update  the privacy statement for SuperDutyDrive.com in order to reflect any changes to  the website or our privacy practices.  If  we update this statement, the new statement will be posted to the website ten  (10) days prior to the changes taking effect.          </p>
<p>The  effective date of this privacy statement is 11/25/2015.    </p>
</body>
</html>