<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="ford-fonts/stylesheet.css">
</head>

<body style="font-family:franklin_gothic_fsbook;">

<h1>Your California Privacy Rights</h1>

<!-- Begin California Privacy Policy Insert -->
<SCRIPT language=JavaScript src="http://www.ford.com/go/privacy/include.js"></SCRIPT>
<NOSCRIPT>View <A HREF=" http://www.ford.com/help/privacy/california/ ">Your California Privacy Rights</A>.</NOSCRIPT>
<!-- End California Privacy Policy Insert -->


</body>
</html>