<?php session_start(); unset($_SESSION['errors']); unset($_SESSION['form']); ?>

<!doctype html>
<html>
<head>
<base target="_parent" />
<meta charset="utf-8">
<title>2017 Ford Super Duty Drive Tour</title>
<link rel="stylesheet" type="text/css" href="styles/style.css"/>
<link rel="stylesheet" type="text/css" href="ford-fonts/stylesheet.css">
<link rel="stylesheet" type="text/css" href="styles/colorbox.css"/>
<script src="jquery/js/jquery-1.10.2.js"></script>
<script src="jquery/js/jquery-ui-1.10.4.custom.js"></script>
<script src="jquery/js/jquery.colorbox.js"></script>

<script>
$(document).ready(function(){
	$(".privacy").colorbox({iframe:true, width:"70%", height:"70%", opacity: 0.4});
});

$(document).ready(function(){
	$(".caprivacy").colorbox({iframe:true, width:"70%", height:"60%", opacity: 0.4});
});

$(document).ready(function(){
	$(".rules").colorbox({iframe:true, width:"70%", height:"60%", opacity: 0.4});
});
</script>
</head>

<body style="background-color: #000;">

<div id="topbar"></div>

    	<div id="header">
        
            <a href="index.php"><div id="logo"></div></a>
        
            <div id="tagline"></div> 
        
        </div>

        <div id="formarea" style="height: 760px; background-color: #222222;">
        
        	<div id="contentform" style="text-align:center; height: 300px;">
            
				<div class="headers-white-thanks">THANKS FOR REGISTERING.</div>

                <div class="small-headers-white" style="margin-top: 20px;">You will be hearing back from us soon.</div>
                
                <div class="small-headers-orange" style="margin-top: 60px;">Learn more about the toughest, smartest, most capable<br>Super Duty<sup>&reg;</sup> ever by clicking below.</div>
                <a href="http://www.ford.com/trucks/superduty/2017/" target="_blank"><img src="images/learn-more.jpg" width="204" height="70" alt="" style="margin-top: 100px;"/></a>
                
            
            </div>
       
        </div>
        
	<?php include('includes/footer.php'); ?>           

</body>
</html>