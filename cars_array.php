<?php 

///////// vehicles
$cars = array('2015 C-MAX Energi.png' => 'C-MAX Energi', '2015 C-MAX Hybrid.png' => 'C-MAX Hybrid', '2015 Edge.png' => 'Edge', '2015 Fiesta.png' => 'Fiesta', '2015 Focus Electric.png' => 'Focus Electric', '2015 Focus ST.png' => 'Focus ST', '2015 Focus.png' => 'Focus', '2015 Fusion Energi.png' => 'Fusion Energi', '2015 Fusion Hybrid.png' => 'Fusion Hybrid', '2015 Taurus.png' => 'Taurus');

echo '<div style="width: 60%; height: 200px;">';

foreach ($cars as $key => $value){
	
	echo '<div style="width: 200px; height: 180px; float: left; text-align: center; margin-right: 12px; cursor: pointer;"><div><img src="vehicles/'.$key.'"></div><div style="background-color: #96979a; color: #fff; text-transform:uppercase; border: 2px solid; border-bottom-left-radius: 10px; border: 2px solid; border-bottom-right-radius: 10px; cursor: pointer; height: 30px; padding-top: 15px;">'.$value.'</div></div>';
	
}

echo '</div>';

?>