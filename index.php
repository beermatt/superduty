<?php session_start();

if (empty($_SESSION)) {
    $_SESSION['form']['First_Name'] = "";
    $_SESSION['form']['Email'] = "";
    $_SESSION['form']['Address'] = "";
    $_SESSION['form']['Phone'] = "";
    $_SESSION['form']['Address2'] = "";
    $_SESSION['form']['City'] = "";
    $_SESSION['form']['State'] = "";
    $_SESSION['form']['Zip'] = "";
}


?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
<title>2017 Ford Super Duty Drive Tour</title>
<link rel="stylesheet" type="text/css" href="styles/style.css"/>
<link rel="stylesheet" type="text/css" href="ford-fonts/stylesheet.css">
<link rel="stylesheet" type="text/css" href="styles/colorbox.css"/>
<script src="jquery/js/jquery-1.10.2.js"></script>
<script src="jquery/js/jquery-ui-1.10.4.custom.js"></script>
<script src="jquery/js/smooth-scroll.js"></script>
<script src="jquery/js/jquery.colorbox.js"></script>

<script>
$(document).ready(function(){
    $('.navlink').click(function(){
        $('.navlink').css('color', 'white');
        $(this).css('color', '#C63E25');
    });
});
</script>

<script>
$(document).ready(function(){
	$(".play_video").colorbox({iframe:true, innerWidth:800, innerHeight:500});
});

$(document).ready(function(){
	$(".show-map").colorbox({iframe:true, width: 745, height: 550, opacity:0.5});
});

$(document).ready(function(){
	$(".smartest-duty").colorbox({iframe:true, width: 745, height: 550, opacity:0.5});
});

$(document).ready(function(){
	$(".toughest-duty").colorbox({iframe:true, width: 745, height: 550, opacity:0.5});
});

$(document).ready(function(){
	$(".capable-duty").colorbox({iframe:true, width: 745, height: 550, opacity:0.5});
});

$(document).ready(function(){
	$(".privacy").colorbox({iframe:true, width:"70%", height:"70%", opacity: 0.4});
});

$(document).ready(function(){
	$(".caprivacy").colorbox({iframe:true, width:"70%", height:"60%", opacity: 0.4});
});

$(document).ready(function(){
	$(".rules").colorbox({iframe:true, width:"70%", height:"60%", opacity: 0.4});
});

$(document).ready(function(){
	$(".rules-white").colorbox({iframe:true, width:"70%", height:"60%", opacity: 0.4});
});

$(document).ready(function(){
    $("#Email").on('change keyup paste', function() {
        if($("#Optin").prop( "checked" ) == false){
            $(".subtext").css('visibility', 'visible');
        }
    });
    $('#Optin').change(function(){
        if($("#Optin").prop( "checked" ) == true){
            $(".subtext").css('visibility', 'hidden');
        } else {
            $(".subtext").css('visibility', 'visible');
        }
    });    
});
</script>
</head>

<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="topbar"></div>

    	<div id="header">
        
            <div id="logo"></div>
        
            <div id="tagline"></div> <div id="nav"><div class="navtext"><a data-scroll href="index.php#event" style="color:inherit; text-decoration: none;" class="navlink">EVENT INFO</a></div> <div class="navtext"><a data-scroll href="index.php#win" style="color:inherit; text-decoration: none;" class="navlink">ENTER TO WIN</a></div> <div class="navtext"><a data-scroll href="index.php#featuresarea" style="color:inherit; text-decoration: none;" class="navlink">SUPER DUTY<sup>&reg;</sup> VIDEOS</a></div></div>
        
        </div>
        
        <div id="video">

            <div id="videooverlay"></div>
        
        	<div style="z-index:2; background-color: green; position:absolute; height: 100%;"></div>
        
            <div id="future"></div>
            
            <div id="enter"><a data-scroll href="#win" style="color:inherit; text-decoration: none;">ENTER TO WIN</a></div>
            
            <div id="enter" style="margin-top: -30px;"><a href="media/1873_superduty_web_teaser_v6_digitalteam.mp4" class="play_video"><img src="images/play_video_btn.png" width="191" height="54" alt="" border="0"/></a></div>
            
            <div id="scroll">SCROLL</div>
            
            <div id="scroll-arrow"><a data-scroll href="#event" style="color:inherit; text-decoration: none;"><img src="images/d-arrow.png" width="50" height="46" alt=""/></a></div>
            
            <div id="disclaimer" style="width: 500px;">Available in dealerships late 2016. Pre-production vehicle shown.</div>
            
            <video autoplay loop id="bgvid">
            <source src="media/superduty_loop_test.webm" type="video/webm">
            <source src="media/superduty_loop_test_1.mp4" type="video/mp4">
            </video>
       
        </div>
        
        <span id="event"></span>
        
        <div id="maincontent">
        
        	<div id="contentdiv">
            
            	<div class="headers">EVENT INFO</div>
                
                <div class="maincontent"><strong>Super Duty<sup style="font-size:75%">&reg;</sup> Tour:</strong> A limited opportunity to drive the All-New 2017 Ford Super Duty<sup style="font-size:75%">&reg;</sup> before it is available at local Ford Dealerships. Register below to receive information on when the tour will be in your area and for the sweepstakes*.<br><br>
                
<span style="font-size: 14px;">*No purchase necessary. Open to licensed drivers 18 and older.  Sweeps end 12/1/16. Complete rules available <a href="rules.php" class="rules">here</a>. Tour schedule to be announced early 2016.</span></div> 

<div style="float:right; margin-top: 22px; height: 350px;"><img src="images/super-duty.jpg" alt=""/></div>

<div style="float:right; margin-top: 30px; margin-right: 60px;"><a href="images/map.jpg" class="show-map"><img src="images/tour-map.jpg" alt="" border="0"/></a></div>
                
                
            </div>
        
        </div>
        
        
        <span id="win"></span>
        
        <div id="formarea">
        
        	<div id="contentform">
            	
                <div class="headers-white">ENTER NOW FOR YOUR CHANCE TO<br> WIN A 2017 FORD SUPER DUTY<sup style="font-size:65%">&reg;</sup></div>
                
                <div class="maincontentform">Register for a chance to take home your own 2017 Ford Super Duty<sup style="font-size:75%">&reg;</sup> and redefine tough in your neighborhood.<br>

We will notify you when the Drive Tour is in your area so you can be one of the first to drive the Super Duty<sup style="font-size:75%">&reg;</sup> Future of Tough!<br><br>

<span style="font-size: 12px;">No purchase necessary. Open to licensed drivers 18 and older. Sweeps ends 12/1/16. Complete rules available <a href="rules.php" class="rules-white">here</a>.</span></div>

<form action="post_reg.php" method="post" name="signup" id="signup">

<input type="text" placeholder="FULL NAME" name="First_Name" <?php if(isset($_SESSION['errors']['First_Name'])){ echo 'style="background-color: #e9a6a6;"'; } ?> value="<?php echo $_SESSION['form']['First_Name']; ?>">
<input type="text" placeholder="EMAIL ADDRESS" name="Email" id="Email"<?php if(isset($_SESSION['errors']['Email'])){ echo 'style="background-color: #e9a6a6;"'; } ?> value="<?php echo $_SESSION['form']['Email']; ?>">
<input type="text" placeholder="ADDRESS" name="Address" <?php if(isset($_SESSION['errors']['Address'])){ echo 'style="background-color: #e9a6a6;"'; } ?> value="<?php echo $_SESSION['form']['Address']; ?>">
<input type="text" placeholder="PRIMARY PHONE" name="Phone" <?php if(isset($_SESSION['errors']['Phone'])){ echo 'style="background-color: #e9a6a6;"'; } ?> value="<?php echo $_SESSION['form']['Phone']; ?>">
<input type="text" placeholder="APT, UNIT, SUITE" name="Address2" value="<?php echo $_SESSION['form']['Address2']; ?>">
<input type="text" placeholder="CITY" name="City" <?php if(isset($_SESSION['errors']['City'])){ echo 'style="background-color: #e9a6a6;"'; } ?> value="<?php echo $_SESSION['form']['City']; ?>">
<input type="text" placeholder="STATE" name="State" <?php if(isset($_SESSION['errors']['State'])){ echo 'style="background-color: #e9a6a6;"'; } ?> value="<?php echo $_SESSION['form']['State']; ?>">
<input type="text" placeholder="ZIP" name="Zip" <?php if(isset($_SESSION['errors']['Zip'])){ echo 'style="background-color: #e9a6a6;"'; } ?> value="<?php echo $_SESSION['form']['Zip']; ?>">

<br><br>

<div style="height: 30px; float:left;"><input type="checkbox" style="zoom:2.0; margin-left: -1px; float:left;" name="Optin" id="Optin" value="Yes" <?php if(isset($_SESSION['form']['Optin'])){ echo 'checked'; } ?>></div><div class="smallform">Yes! Please email me communications, including product information, offers and incentives from Ford Motor Company and the local Dealer.</div>
<div style="clear:both;"></div>
<div class="subtext" style="color: red;">We’ll keep you updated on the Drive the Future of Tough Tour.  Check this box to hear about the special offers and incentives!</div>



<div style="height: 30px; float:left;"><input type="checkbox" style="zoom:2.0; margin-left: -1px; float:left; <?php if(isset($_SESSION['errors']['Age'])){ echo 'background-color: #e9a6a6;'; } ?>" name="Age" value="Yes" <?php if(isset($_SESSION['form']['Age'])){ echo 'checked'; } ?>></div><div class="smallform">Yes, I am 18 years or older with a valid driver's license. <?php if(isset($_SESSION['errors']['Age'])){ echo '&nbsp;&nbsp;<span style="color: red;">Required</span>'; } ?></div>

<div style="clear:both;"></div>
<br>


<div style="height: 25px; float:left;"><input type="checkbox" style="zoom:2.0; margin-left: -1px; float:left;" name="terms_and_conditions" value="Yes" <?php if(isset($_SESSION['form']['terms_and_conditions'])){ echo 'checked'; } ?>></div><div class="smallform">Yes, I have read the official rules. <?php if(isset($_SESSION['errors']['terms_and_conditions'])){ echo '&nbsp;&nbsp;<span style="color: red;">Required</span>'; } ?></div>

<div style="margin-top: 40px; margin-left: 260px; width: 350px;"><img src="images/submit.png" width="343" height="62" alt="" class="submitableimage" style="cursor: pointer;"/></div>

</form>

<script>
   $('img.submitableimage').click(function(){
      $('#signup').submit();
   });
</script>
            
            </div>
       
        </div>
        
        
        
        <span id="featuresarea"></span>
        
        <div id="features">
        
        	<div id="contentfeatures">
        
        	<div class="headers-white">SUPER DUTY<sup>&reg;</sup> VIDEOS</div>
            
                <div style="margin-top: 30px;">
                
                <a href="media/super_duty_smart_long_version_w_disclaimer_1.webm" class="smartest-duty"><img src="images/videoscreenshoot_01.jpg" width="298" height="198" alt="" style="margin-right: 20px;" border="0"/></a>
                
                <a href="media/ts_ford_frametest_fulllength_ford-com_3_4.webm" class="toughest-duty"><img src="images/videoscreenshoot_02.jpg" width="298" height="198" alt="" style="margin-right: 20px;"/></a>
                
                <a href="media/super_duty_capable_w_disclaimer_1.webm" class="capable-duty"><img src="images/videoscreenshoot_03.jpg" width="299" height="198" alt=""/></a>
                
                </div>
                
                <div class="imgheaders">THE SMARTEST <br>SUPER DUTY EVER<br><br><span style="font-size: 14px; font-weight:100;">There are seven available cameras to help you see the world around you. And that’s just the start of the smart.</span></div>
                
                <div class="imgheaders">THE TOUGHEST  <br>SUPER DUTY EVER<br><br><span style="font-size: 14px; font-weight:100;">You’ve heard about the fully-boxed, high-strength steel frame of the Ford Super Duty. But you’ve never seen a test as tough as this.</span></div>
                
                <div class="imgheaders" style="width: 250px; padding-left: 10px;">THE MOST CAPABLE <br>SUPER DUTY EVER<br><br><span style="font-size: 14px; font-weight:100;">High-strength, military-grade, aluminum alloy body. High-strength steel frame. 12 million miles of testing. Bring on the work.</span></div>
            
            </div>
        
        </div>
        
	<?php include('includes/footer-home.php'); ?>   
 
<script>
smoothScroll.init();
</script>

</body>
</html>